const btnCalcular = document.getElementById('btnCalcular');
btnCalcular.addEventListener('click',function(){
    let valorAltura = document.getElementById('valorAltura').value;
    let valorPeso = document.getElementById('valorPeso').value;

    let CalculoIMC = valorPeso/(valorAltura*valorAltura);

    document.getElementById('txtIMC').innerHTML = CalculoIMC.toFixed(2);
    sexoImagen(CalculoIMC);
    totalCalorias(valorPeso);
})

function sexoImagen(CalculoIMC){
    let img=document.getElementById('imagen')
    let sexoM=document.getElementById('radMas').checked
    let sexoF=document.getElementById('radFem').checked

    if(sexoM){
    if(CalculoIMC<18.55) img.src="../img/01H.png"
    if(CalculoIMC>18.55) img.src="../img/02H.png"
    if(CalculoIMC>25) img.src="../img/03H.png"
    if(CalculoIMC>29.99) img.src="../img/04H.png"
    if(CalculoIMC>35) img.src="../img/05H.png"
    if(CalculoIMC>40) img.src="../img/06H.png"
    }

    if(sexoF){
        if(CalculoIMC<18.55) img.src="../img/01M.png"
        if(CalculoIMC>18.55) img.src="../img/02M.png"
        if(CalculoIMC>25) img.src="../img/03M.png"
        if(CalculoIMC>29.99) img.src="../img/04M.png"
        if(CalculoIMC>35) img.src="../img/05M.png"
        if(CalculoIMC>40) img.src="../img/06M.png"
        }
}

    function totalCalorias(valorPeso){
    let edad=document.getElementById('valorEdad').value
    let calorias=0;
    let sexoM=document.getElementById('radMas').checked
    let sexoF=document.getElementById('radFem').checked

        if(sexoM){
           if(edad<18) calorias = (17.686 * valorPeso) + 692.2
           if(edad>=18) calorias = (15.057 * valorPeso) + 692.2
           if(edad>30) calorias = (11.472 * valorPeso) + 873.1
           if(edad>60) calorias = (11.711 * valorPeso) + 587.7
        }
        if(sexoF){
            if(edad<18) calorias = (13.384 * valorPeso) + 692.6
            if(edad>=18) calorias = (14.818 * valorPeso) + 486.6
            if(edad>30) calorias = (8.126 * valorPeso) + 845.6
            if(edad>60) calorias = (9.082 * valorPeso) + 658.5
        }
        document.getElementById('txtCalorias').value=calorias.toFixed(2)
      }