// Obtener el objeto boton de calcular

const btnCalcular = document.getElementById('btnCalcular');

btnCalcular.addEventListener('click',function(){

    let valorAuto = document.getElementById('txtValorAuto').value;
    let porcentaje = document.getElementById('txtPorcentaje').value;
    let plazo = document.getElementById('plazos').value;
    
    // Hacer calculos
    let pagoInicial = valorAuto * (porcentaje/100);
    let totalFin = valorAuto - pagoInicial;
    let plazos = totalFin/plazo

    // Mostrar datos
    document.getElementById('txtPagoInicial').value=pagoInicial;
    document.getElementById('txtTotalFin').value=totalFin;
    document.getElementById('txtPagoMensual').value=plazos

})

//codificar el boton de limpiar


    let limpiar = document.getElementById('btnLimpiar');
    limpiar.addEventListener('click',()=>{
        document.getElementById('txtValorAuto').value = "";
        document.getElementById('txtPorcentaje').value = "";
        document.getElementById('txtPagoInicial').value = "";
        document.getElementById('txtTotalFin').value = "";
        document.getElementById('txtPagoMensual').value = "";
    })  

